import React from "react";
import { useState } from "react";
import {useNavigate} from "react-router-dom";
import user from "../../data/user.json";
import CSS from "./style.css"

const LOCAL_STORAGE_KEY = "isLoggedIn";
const RegisterCard = () =>{
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [confirmPassword, setConfirmPassword] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);
    
    const navigate = useNavigate();

    const handleFormRegister = (e) =>{
        e.preventDefault();

        if (username !== user.username) {
            setErrorMessage("Username is not valid or registered");
            return;
        }
        if (password !== user.password) {
            setErrorMessage("Password is wrong");
            return;
        }

        localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(true));
        navigate("/");

    }
    return(
        <div className={`container bg-light d-flex flex-column align-items-center justify-content-center`}>
        <div className={`row`}>
          <div className={`col-12 mb-4`}>
            <h1 className={`text-login`}>Register</h1>
          </div>
        </div>
        <div className={`row input-form`}>
          <form onSubmit={handleFormRegister}>
            <div className={`col-4 my-3`}>
              <input 
              className={`form-control`} 
              type="text" name="username" 
              placeholder="Input your username" 
              onChange={(e)=> setUsername(e.target.value)}/>
            </div>
            <div class="col-4 my-3">
              <input 
              type="password" 
              className={`form-control`} 
              name="password" 
              placeholder="Input your password"
              onChange={(e)=> setPassword(e.target.value)}/>
            </div>
            <div className={`col-4 my-3`}>
              <input 
              type="password" 
              className={`form-control`}  
              name="password" 
              placeholder="Confirm your password"
              onChange={(e)=> setConfirmPassword(e.target.value)}/>
            </div>
            <div className={`col-4`} >
              <input className={`btn btn-warning`} type="submit" name="" value="Register"/>
            </div>
          </form>  
        </div>
      </div>
    );
}

export default RegisterCard;