import React from "react";
import { useState } from "react";
import {useNavigate} from "react-router-dom";
import user from "../../data/user.json";
import CSS from "./style.css"
const LoginCard = () =>{
    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [errorMessage, setErrorMessage] = useState(null);
    
    const navigate = useNavigate();

    const handleFormLogin = (e) =>{
        e.preventDefault();

        if (username === user.username && password === user.password) {
          navigate("/"); 
        }else{
          alert("Your username or password is not valid");
        }
    }

    return(
      <div className={`container bg-light d-flex flex-column align-items-center justify-content-center`}>
      <div className={`row`}>
        <div className={`col-12 mb-4`}>
          <h1 className={`text-login`}>Login</h1>
        </div>
      </div>
      <div className={`row input-form`}>
        <form onSubmit={handleFormLogin}>
          <div className={`col-4 my-3`}>
            <input 
            className={`form-control`} 
            type="text" name="username" 
            placeholder="Input your username" 
            onChange={(e)=> setUsername(e.target.value)}/>
          </div>
          <div class="col-4 my-3">
            <input 
            type="password" 
            className={`form-control`} 
            name="password" 
            placeholder="Input your password"
            onChange={(e)=> setPassword(e.target.value)}/>
          </div>
          <div className={`col-4`} >
            <input className={`btn btn-warning`} type="submit" name="" value="Login"/>
          </div>
        </form>  
      </div>
    </div>
    );
}

export default LoginCard;